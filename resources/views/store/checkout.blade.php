@extends('layouts.default')

@section('content')
	<h2 class="header">Checkout</h2>

	<ul class="tabs tabs-fixed-width">
		<li class="tab"><a href="#step1">Suas informações</a></li>
		<li class="tab"><a href="#step2">Entrega</a></li>
		<li class="tab"><a href="#step3">Pagamento</a></li>
	</ul>
	
	<form action="/checkout/{{ $id }}" method="post" id="form">
		
		{{ csrf_field() }}

		<input type="hidden" name="itemId1" value="0001">
		<input type="hidden" name="itemDescription1" value="Produto de Teste 1">
		<input type="hidden" name="itemAmount1" value="250.00">
		<input type="hidden" name="itemQuantity1" value="2">

		<div id="step1">
			<p>Preencha suas informações</p>

            <input type="hidden" name="senderHash" id="senderHash">

			<div class="row">
				<div class="input-field col s12">
					<input type="text" name="senderName" id="senderName">
					<label for="senderName">Nome Completo</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<input type="text" name="senderCPF" id="senderCPF">
					<label for="senderCPF">CPF</label>
				</div>
				<div class="input-field col s6">
					<input type="text" name="senderEmail" id="senderEmail">
					<label for="senderEmail">E-mail</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6 offset-s3">
					<input type="text" name="senderPhone" id="senderPhone">
					<label for="senderPhone">Telefone</label>
				</div>
			</div>
		</div>
		<div id="step2">
			<p>Informe os dados para a entrega</p>

			<div class="row">
				<div class="input-field col s6">
					<input type="text" name="shippingAddressPostalCode" id="shippingAddressPostalCode">
					<label for="shippingAddressPostalCode">CEP</label>
				</div>
				<div class="input-field col s6">
					<input type="text" name="shippingAddressStreet" id="shippingAddressStreet">
					<label for="shippingAddressStreet">Logradouro</label>
				</div>
			</div>	
			<div class="row">
				<div class="input-field col s6">
					<input type="text" name="shippingAddressNumber" id="shippingAddressNumber">
					<label for="shippingAddressNumber">Número</label>
				</div>
				<div class="input-field col s6">
					<input type="text" name="shippingAddressComplement" id="shippingAddressComplement">
					<label for="shippingAddressComplement">Complemento</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<input type="text" name="shippingAddressDistrict" id="shippingAddressDistrict">
					<label for="shippingAddressDistrict">Bairro</label>
				</div>
				<div class="input-field col s6">
					<input type="text" name="shippingAddressCity" id="shippingAddressCity">
					<label for="shippingAddressCity">Cidade</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<input type="text" name="shippingAddressState" id="shippingAddressState">
					<label for="shippingAddressState">Estado</label>
				</div>
				<div class="col s6">
					<input type="hidden" name="shippingCost" value="21.50">
					<select name="shippingType" id="shippingType" class="browser-default">
						<option disabled selected>Forma de entrega</option>]
						<option value="1">Encomenda normal (PAC)</option>
						<option value="2">SEDEX</option>
						<option value="3">Frete próprio</option>
					</select>
				</div>
			</div>
		</div>
		<div id="step3">
			<p>Preencha os dados de pagamento</p>
			
			<input type="hidden" name="creditCardToken" id="creditCardToken">
			<input type="hidden" name="installmentValue" id="installmentValue">

			<div class="row">
				<div class="input-field col s9">
					<input type="text" id="cardNumber">
					<label for="cardNumber">Número do cartão</label>
					<div id="_card_brand"></div>
				</div>
				<div class="input-field col s3">
					<input type="text" id="cvv">
					<label for="cvv">Código de Segurança</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s4">
					<input type="text" id="expirationMonth">
					<label for="expirationMonth">Mês de Expiração</label>
				</div>
				<div class="input-field col s4">
					<input type="text" id="expirationYear">
					<label for="expirationYear">Ano de Expiração</label>
				</div>
				<div class="col s4">
					<select id="installmentQuantity" name="installmentQuantity" class="browser-default">
						<option disabled selected>Parcelamento</option>
					</select>
				</div>
			</div>

			<p>Dados do dono do cartão</p>

			<p>
				<input type="checkbox" id="copy_from_me">
				<label for="copy_from_me">Copiar seus dados</label>
			</p>

			<div id="holder_data">
				<div class="row">
					<div class="input-field col s12">
						<input type="text" name="creditCardHolderName" id="creditCardHolderName">
						<label for="creditCardHolderName">Nome Completo</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s6">
						<input type="text" name="creditCardHolderCPF" id="creditCardHolderCPF">
						<label for="creditCardHolderCPF">CPF</label>
					</div>
					<div class="input-field col s6">
						<input type="text" name="creditCardHolderPhone" id="creditCardHolderPhone">
						<label for="creditCardHolderPhone">Telefone</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					<input type="text" name="creditCardHolderBirthDate" id="creditCardHolderBirthDate">
					<label for="creditCardHolderBirthDate">Data de nascimento</label>
				</div>
			</div>

			<p>Endereço da fatura</p>

			<p>
				<input type="checkbox" id="copy_from_shipping">
				<label for="copy_from_shipping">Copiar do endereço de entrega</label>
			</p>

			<div id="shipping_data">
				<div class="row">
					<div class="input-field col s6">
						<input type="text" name="billingAddressPostalCode" id="billingAddressPostalCode">
						<label for="billingAddressPostalCode">CEP</label>
					</div>
					<div class="input-field col s6">
						<input type="text" name="billingAddressStreet" id="billingAddressStreet">
						<label for="billingAddressStreet">Logradouro</label>
					</div>
				</div>	
				<div class="row">
					<div class="input-field col s6">
						<input type="text" name="billingAddressNumber" id="billingAddressNumber">
						<label for="billingAddressNumber">Número</label>
					</div>
					<div class="input-field col s6">
						<input type="text" name="billingAddressComplement" id="billingAddressComplement">
						<label for="billingAddressComplement">Complemento</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s6">
						<input type="text" name="billingAddressDistrict" id="billingAddressDistrict">
						<label for="billingAddressDistrict">Bairro</label>
					</div>
					<div class="input-field col s6">
						<input type="text" name="billingAddressCity" id="billingAddressCity">
						<label for="billingAddressCity">Cidade</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s6">
						<input type="text" name="billingAddressState" id="billingAddressState">
						<label for="billingAddressState">Estado</label>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<input type="submit" value="pagar" class="btn">
				</div>
			</div>
			<br><br>
		</div>
	</form>

	
	<div id="payment_methods" class="center-align"></div>
@endsection

@section('scripts')
	<script type="text/javascript" src=
	"https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js">
	</script>
	<script type="text/javascript" src="/js/pagseguro.js"></script>

	<script type="text/javascript">
		const paymentData = {
			brand: '',
			amount: {{ $amount }},
		}

		PagSeguroDirectPayment.setSessionId('{!! $session !!}');

		pagSeguro.getPaymentMethods(paymentData.amount)
			.then(function (urls) {
				let html = '';

				urls.forEach(function (url) {
					html += '<img src="' + url + '" class="credit_card">'
				});

				$('#payment_methods').html(html);
			});

        $('#senderName').on('change', function () {
            pagSeguro.getSenderHash()
                .then(function (data) {
                    $('#senderHash').val(data);
                })
        });    

		$('#shippingAddressPostalCode').on('keyup', function () {
			let cep = $(this).val();
			if (cep.length == 8) {
				$.get('https://viacep.com.br/ws/' + cep + '/json/')
					.then(function (response) {
						$('#shippingAddressDistrict').val(response.bairro);
						$('#shippingAddressCity').val(response.localidade);
						$('#shippingAddressStreet').val(response.logradouro);
						$('#shippingAddressState').val(response.uf);
						Materialize.updateTextFields();
					});
			}
		});

		$('#cardNumber').on('keyup', function() {
			if ($(this).val().length >= 6) {
				let bin = $(this).val();
				pagSeguro.getBrand(bin)
					.then(function (res) {
						paymentData.brand = res.result.brand.name;
						$('#_card_brand').html('<img src="' + res.url + '" class="credit_card">')
						return pagSeguro.getInstallments(paymentData.amount, paymentData.brand);
					})
					.then(function (res) {
						let html = '';
						res.forEach(function (item) {
							html += '<option value="' + item.quantity + '">' + item.quantity +'x R$' + item.installmentAmount + ' - total R$ ' + item.totalAmount + '</option>'
						});

						$('#installmentQuantity').html(html);
						$('#installmentValue').val(res[0].installmentAmount);
						$('#installmentQuantity').on('change', function () {
							let value = res[$('#installmentQuantity').val() - 1].installmentAmount;
							$('#installmentValue').val(value);
						});
					})
			}
		});

		$('#form').on('submit', function(e) {
			e.preventDefault();
			let params = {
				cardNumber: $('#cardNumber').val(),
				cvv: $('#cvv').val(),
				cardNumber: $('#cardNumber').val(),
				expirationMonth: $('#expirationMonth').val(),
				expirationYear: $('#expirationYear').val(),
				brand: paymentData.brand
			}

			pagSeguro.createCardToken(params)
				.then(function (token) {
					$('#creditCardToken').val(token);

					let url = $('#form').attr('action');
					let data = $('#form').serialize();
					$.post(url,data)
                    .then(function () {
                        window.location = '/checkout/success';
                    });
				});
		});

		let toggle = function(element, verification, callbackShow, callbackHide) {
			if (!verification.is(':checked')) {
				$(element).show();
				callbackShow()
			}else {
				$(element).hide();
				callbackHide()
			}
		}

		let holderShow = function () {
			$('#creditCardHolderName').val('');
			$('#creditCardHolderCPF').val('');
			$('#creditCardHolderPhone').val('');
		}

		let holderHide = function () {
			$('#creditCardHolderName').val($('#senderName').val());
			$('#creditCardHolderCPF').val($('#senderCPF').val());
			$('#creditCardHolderPhone').val($('#senderPhone').val());
		}

		let shippingShow = function () {
			$('#billingAddressPostalCode').val('');
			$('#billingAddressStreet').val('');
			$('#billingAddressNumber').val('');
			$('#billingAddressComplement').val('');
			$('#billingAddressDistrict').val('');
			$('#billingAddressCity').val('');
			$('#billingAddressState').val('');
		}

		let shippingHide = function () {
			$('#billingAddressPostalCode').val($('#shippingAddressPostalCode').val());
			$('#billingAddressStreet').val($('#shippingAddressStreet').val());
			$('#billingAddressNumber').val($('#shippingAddressNumber').val());
			$('#billingAddressComplement').val($('#shippingAddressComplement').val());
			$('#billingAddressDistrict').val($('#shippingAddressDistrict').val());
			$('#billingAddressCity').val($('#shippingAddressCity').val());
			$('#billingAddressState').val($('#shippingAddressState').val());
		}

		toggle('#holder_data', $(this), holderShow, holderHide);
		toggle('#shipping_data', $(this), shippingShow, shippingHide);

		$('#copy_from_me').on('change',function() {
			toggle('#holder_data', $(this), holderShow, holderHide);
		});

		$('#copy_from_shipping').on('change',function() {
			toggle('#shipping_data', $(this), shippingShow, shippingHide);
		});
	</script>
@endsection