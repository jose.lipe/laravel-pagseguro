<!DOCTYPE html>
<html>
<head>
	<title>Laravel Pagseguro</title>

	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<style type="text/css">
		.credit_card {
			border: 1px solid #bbb;
		}
	</style>    
</head>
<body>
	<header id="header" class="row">
		<nav>
			<div class="nav-wrapper col s12 black">
				<a href="" class="brand-logo">Loja Teste</a>
				<ul class="right">
					<li><a href="">Minha conta</a></li>
					<li><a href="">Ajuda</a></li>
					<li><a href="">Sair</a></li>
				</ul>
			</div>
		</nav>
		<div class="parallax-container">
			<div class="parallax">
				<img src="/img/iphone.jpg">
			</div>
		</div>
	</header>
	<main>
		<section class="container">
			@yield('content')
		</section>
	</main>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.parallax').parallax();
		});
	</script>
	@yield('scripts')
</body>
</html>